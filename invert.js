// function to invert an object
function invert(obj) {

    // New inverted object
    let newObj = {};

    for (let key in obj) {

        // Make value key and key the value
        newObj[obj[key]] = key;
    }

    return newObj;
}

// export the function
export {invert}