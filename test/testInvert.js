import { invert } from "../invert.js";

// test object
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// Testing the function
let ans = invert(testObject);

// Pint the result
console.log(ans);