// Function to return keys as string

function keys(obj) {

    // It will store all the keys
    let keysArray = [];

    for (let key in obj) {
        keysArray.push(String(key))
    }

    return keysArray;
}

// Export the function
export {keys};