// Function that fill undefined properties
function defaults(obj, defaultProps) {

    for (let key in defaultProps) {

        // if that value is undefined in object set to default values
        if (typeof(obj[key]) == 'undefined') {
            obj[key] = defaultProps[key];
        }
    }

    return obj;
}

// Export the function
export {defaults};