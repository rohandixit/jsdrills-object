// funtion to return pairs from object.
function pairs(obj) {

    // Pairs will be stored in this list
    let pairsArray = [];

    for (let key in obj) {
        pairsArray.push([key, obj[key]]);
    }

    return pairsArray;

}

// Export the function
export {pairs};
