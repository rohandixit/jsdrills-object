// Function to return values of an object
function values(obj) {

    // Array to store values of an object.
    let valueArray = [];

    for (let key in obj){
        valueArray.push(obj[key]);
    }

    return valueArray;
}

// Export the function
export {values};