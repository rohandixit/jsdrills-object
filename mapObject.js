// Map Object function for objects
function mapObjects(obj, cb) {

    for (let key in obj) {
        obj[key] = cb(obj[key]);
    }

    return obj
}

// Export the function
export {mapObjects};